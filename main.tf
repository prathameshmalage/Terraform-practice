terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.26.0"
    }
  }
}

provider "google" {
    project     = var.project_id
    region      = var.region
}


resource "google_compute_network" "vpc_netwrok" {
  name = "first-vpc"
  description = "Creating this VPC for learning purpose"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "public-subnetwork" {
    name = "terraform-subnetwork"
    ip_cidr_range = "10.2.0.0/16"
    region = "us-central1"
    network = google_compute_network.vpc_netwrok.name
}