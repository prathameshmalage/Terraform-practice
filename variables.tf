variable "project_id"{
    description     = "Project ID"
    default         = "prathamesh-kubernetes"
    type            = string
    
}

variable "region"{
    description     = "Region of Project"
    default         = "us-central1"
    type            = string
}